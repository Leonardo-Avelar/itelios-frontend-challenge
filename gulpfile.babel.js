'use strict';

import gulp    from 'gulp';
import connect from 'gulp-connect';
import glob    from 'glob';

const dirs = {
  src: 'src',
  dest: 'build'
};

gulp.task('connect', () => {
  connect.server({
      root: dirs.dest,
      livereload: true
  })
});

/**
 * Carrega os módulos
 */
glob.sync('./gulp-tasks/*.js', {}).forEach( (file) => {
    require(file)(gulp, dirs)
})

// gulp.task('styles', () => {
//     console.log('teste')
// })

gulp.task('default', ['connect', 'styles']);