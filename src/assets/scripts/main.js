
function getDados(callback) {
  var xmlreq = new XMLHttpRequest();
  var url = "/products.json";
  
  xmlreq.open('GET', url, true);
  
  xmlreq.onreadystatechange = function(produtosJson) {
    if (xmlreq.readyState == 4) {

      if (xmlreq.status == 200) {
        var data = JSON.parse(xmlreq.responseText);

        if( typeof callback == 'function' ) {
            callback(data);
        }
      } else {
        console.log("Erro: " + xmlreq.statusText); 
      } 
    }
  };
  
  xmlreq.send(null);
}

function montarSlider() {

  getDados(function(resultado) {
    var produtos = resultado;
    console.log(produtos);
    var shelf = document.querySelector('.shelf__carousel');
    var output = "";

    function mask(text, length) {
      if(text.length > length) {
        text = text.slice(0, length) + "...";
      }
      return text;
    }

    var productVisited = mask(produtos[0].data.item.name, 25);
    document.querySelector('.product-visited').innerText = productVisited;
    
    for (var i in produtos[0].data.recommendation) {
      var imageName = produtos[0].data.recommendation[i].imageName;
      var name     = produtos[0].data.recommendation[i].name;
      var oldPrice = produtos[0].data.recommendation[i].oldPrice;
      var price    = produtos[0].data.recommendation[i].price;
      var productInfo = produtos[0].data.recommendation[i].productInfo.paymentConditions;

      name = mask(name, 87);

      function filter(text) {
        text = text.replace('ou até ', '');
        text = text.replace(' sem juros', '');

        return text;
      }
      productInfo = filter(productInfo);

      if(oldPrice == null) {
        oldPrice = '<span> </span>';
      } else {
        oldPrice = 'De: <span>' + oldPrice + '</span> ';
      }
      
      if (i = 0) {
        output += "<div class='product product--bl-0'>";
      } else {
        output += "<div class='product'>";
      }
      // if (imageName == '/assets/images/iphone-6s-apple-com-tela-47-hd-32gb-3d-touch-ios-9-sensor-touch-id-camera-isight-12mp-wi-fi-4g-gps-bluetooth-e-nfc-ouro-rosa-10404669.jpg') {
      //   imageName = 'assets/images/produto-sem-imagem.jpg';
      // }
      output +=   "<img class='product__image' src='" + imageName + "' />";
      output +=   "<span class='product__name'>"  + name  + "</span>";
      output +=   "<span class='product__oldPrice'>" + oldPrice + "</span>";
      output +=   "<span class='product__price'>Por: <span>" + price + "</span> </span>";
      output +=   "<span class='product__productInfo'>ou até <span>" + productInfo + "</span> sem juros</span>";
      output +=   "<a class='product__btn-add-to-cart' href=''>Adicionar ao carrinho</a>";
      output += "</div>";
    }
    shelf.innerHTML += output;

    function produtoSemFoto() {
      var imageProcuct = document.querySelectorAll('.product__image');

      imageProcuct.forEach(function(element) {

        element.onerror = function(event) {
          console.log(this);
          this.src = 'assets/images/produto-sem-imagem.jpg';
        }
      });
    }
    
    produtoSemFoto();
    montarSlick();
  });
}

window.onload = function () {
  montarSlider();
};