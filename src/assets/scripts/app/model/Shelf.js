class Shelf {

    constructor(title) {
        
        this._title = this._mask(title, 25);
    }

    get title() {
        return this._title;
    }

    _mask(text, length) {
        if(text.length > length)
            text = text.slice(0, length) + "...";

        return text;
    }
}

// Nome principal Shelf

// 1) Fazer a busca do json

// 2) Montar a plateleira
// 3) Validar