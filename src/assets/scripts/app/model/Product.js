class Product {

    constructor(order, image, name, oldPrice, price, info) {
        this._order     =  order;
        this._image     =  this._checkImage(image);
        this._name      =  this._mask(name, 87);
        this._oldPrice  =  this._filterOldPrice(oldPrice);
        this._price     =  price;
        this._info      =  this._filterInfo(info);
    }

    get order() {
        return this._order;
    }

    get image() {
        return this._image;
    }

    get name() {
        return this._name;
    }

    get oldPrice() {
        return this._oldPrice;
    }

    get price() {
        return this._price;
    }

    get info() {
        return this._info;
    }

    set name(name) {
        this._name = name;
    }

    _mask(text, length) {
        if(text.length > length)
            text = text.slice(0, length) + "...";

        return text;
    }

    _checkImage(file) {
        let http = new XMLHttpRequest();
        http.open('HEAD', file, false);
        http.send();

        if (http.status != 404)
            return file;
        else
            return 'assets/images/produto-sem-imagem.jpg';
    }

    _filterInfo(text) {
        text = text.replace('ou até ', '');
        text = text.replace(' sem juros', '');

        return text;
    }

    _filterOldPrice(oldPrice) {
        if(oldPrice == null) {
            oldPrice = '<span> </span>';
        } else {
            oldPrice = `De: <span> ${oldPrice} </span> `;
        }

        return oldPrice;
    }
}