class ShelfView {

    constructor(elemento) {
        this._elemento = elemento;
    }

    _template(product) {
        // console.log(product);

        return `<div class='product'>
                    <img class='product__image' src='                  ${product.image}      ' />
                    <span class='product__name'>                       ${product.name}       </span>
                    <span class='product__oldPrice'>                   ${product.oldPrice}   </span>
                    <span class='product__price'>Por: <span>           ${product.price}      </span> </span>
                    <span class='product__productInfo'>ou até <span>   ${product.info}       </span> sem juros</span>
                    <a class='product__btn-add-to-cart' href=''>Adicionar ao carrinho</a>
                </div>`;
    }

    addTitle(title) {
        this._elemento.innerText = title;
    }

    add(product) {

        this._elemento.innerHTML += this._template(product);
    }
}