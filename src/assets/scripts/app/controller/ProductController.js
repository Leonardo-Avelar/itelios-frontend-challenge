class ProductController {

    constructor() {
        let $ = document.querySelector.bind(document);
        this._shelfView = new ShelfView($('.product-visited'));
        this._productView = new ProductView($('.shelf__carousel'));
        this.obterProdutos();
    }

    obterProdutos() {
        let ajax = new XMLHttpRequest();
        
        ajax.open('GET', '/products.json', true);

        ajax.onreadystatechange = () => {
            
            if (ajax.readyState == 4) {

                if (ajax.status == 200) {
                    let response = JSON.parse(ajax.responseText);

                    let shelf = new Shelf(response[0].data.item.name);
                    this._shelfView.addTitle(shelf.title);

                    response.map(objet => {

                        objet.data.recommendation
                        .forEach((element, indice) => {

                            let order = (indice + 1);

                            let produto = new Product(  
                                                        order,
                                                        element.imageName,
                                                        element.name,
                                                        element.oldPrice,
                                                        element.price,
                                                        element.productInfo.paymentConditions
                                                    )
                            this._productView.add(produto);
                        });
                        Slick.inicializar()
                    })
                } else {
                    console.log("Erro: " + ajax.statusText);
                }
            }
        }

        ajax.send();
    }
}