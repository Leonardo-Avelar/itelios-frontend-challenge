import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';

module.exports = (gulp, dirs) => {
    
    const sassPaths = {
        src: `${dirs.src}/assets/styles/main.scss`,
        dest: `${dirs.dest}/assets/styles/`
    };

    gulp.task('styles', () => {
        return gulp.src(sassPaths.src)
            .pipe(sourcemaps.init())
            .pipe(sass.sync().on('error', sass.logError))
            .pipe(autoprefixer())
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(sassPaths.dest));
    });

    /**
     * SASS Watch
     */
    gulp.task('watchStyles', () => {
        return gulp.watch(src + 'styles/**/*.scss', ['styles'])
        .on('change', (event) => {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...')
        })
    })
}