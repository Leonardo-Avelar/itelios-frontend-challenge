import pug from 'gulp-pug';

module.exports = (gulp, dirs) => {
    
    const pugPaths = {
        src: `${dirs.src}/views/index.pug`,
        dest: `${dirs.dest}/`
    };

    gulp.task('views', () => {
        return gulp.src(pugPaths.src)
            .pipe(pug({ pretty: true }))
            .pipe(pug())
            .pipe(gulp.dest(pugPaths.dest))
            // .pipe(connect.reload())
    });
}