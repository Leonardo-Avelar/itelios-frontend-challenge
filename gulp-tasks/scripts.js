import babel  from 'gulp-babel';
import sourcemaps from 'gulp-sourcemaps';
// import uglify from 'gulp-uglify';

module.exports = (gulp, dirs) => {
    
    const scriptsPaths = {
        src: `${dirs.src}/assets/scripts/app-test/*.js`,
        dest: `${dirs.dest}/assets/scripts/`
    };

    gulp.task('scripts', () => {
        return gulp.src(scriptsPaths.src)
            .pipe(sourcemaps.init())
            .pipe(babel({
                // presets: ['@babel/env']
                // presets: ['@babel/preset-env']
                // presets: ['es2015']
                // presets: [
                //     ["@babel/preset-env", {
                //         "targets": {
                //             "browsers": ["last 2 versions"]
                //         }
                //     }]
                // ]
                "presets": [
                    ["env", {
                        "targets": {
                            "node": "current"
                        }
                    }]
                ]
            }))
            .pipe(sourcemaps.write('.'))
            // .pipe(uglify())
            .pipe(gulp.dest(scriptsPaths.dest));
    });
}