module.exports = (gulp, dirs) => {
    
    const sassPaths = {
        src: `${dirs.src}/assets/styles/main.scss`,
        dest: `${dirs.dest}/assets/styles/`
    };

    gulp.task('dependencies', () => {

        // Json
        gulp.src('./src/assets/products.json')
        .pipe(gulp.dest('./build/'))

        // jQuery
        gulp.src('./node_modules/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('./build/assets/vendor/'))
    
        // Slick
        gulp.src([
            './node_modules/slick-carousel/slick/slick.min.js',
            './node_modules/slick-carousel/slick/slick.css'
        ])
        .pipe(gulp.dest('./build/assets/vendor/'))
    });
}